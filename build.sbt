name := "sendEmail"

version := "0.1"

scalaVersion := "2.13.1"

libraryDependencies ++= Seq(
  "javax.mail" % "mail" % "1.5.0-b01"
)