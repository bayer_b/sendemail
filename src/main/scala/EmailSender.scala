import javax.mail._
import java.util.Properties
import javax.mail.{Message, Session}
import javax.mail.internet.{InternetAddress, MimeMessage}
import scala.io.Source


object EmailSender extends App {

  val host = "smtp.gmail.com"
  val port = "587"

  val address = "bayerb@freemail.hu"
  val username = "balazs.bayer@gmail.com"
  // val password = dbutils.secrets.get("<SECRET_SCOPE>", "EMAIL_SMTP_PASSWORD")
  val password = "***"

  def sendEmail(mailSubject: String, mailContent: String): Unit = {
    val properties = new Properties()
    properties.put("mail.smtp.port", port)
    properties.put("mail.smtp.auth", "true")
    properties.put("mail.smtp.starttls.enable", "true")

    val session = Session.getDefaultInstance(properties, null)
    val message = new MimeMessage(session)
    message.addRecipient(Message.RecipientType.TO, new InternetAddress(address))
    message.setSubject(mailSubject)
    message.setContent(mailContent, "text/html")

    val transport = session.getTransport("smtp")
    transport.connect(host, username, password)
    transport.sendMessage(message, message.getAllRecipients)
  }

sendEmail("teszt", "ez csak egy teszt")



}
